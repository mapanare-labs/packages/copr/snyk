%define debug_package %{nil}

Name:           snyk
Version:        1.1.1294.3
Release:        1%{?dist}
Summary:        Snyk CLI scans and monitors your projects for security vulnerabilities.
Group:          Applications/System
License:        ASL 2.0
URL:            https://snyk.io/
Source0:        https://github.com/%{name}/cli/archive/refs/tags/v%{version}.tar.gz
Source1:        https://github.com/%{name}/cli/releases/download/v%{version}/%{name}-linux

%description
Snyk CLI scans and monitors your projects for security vulnerabilities.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Tue Dec 17 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.1.1294.3

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
